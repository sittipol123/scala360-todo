'use strict';
var validator = require('validate')({
      name: {
        type: 'string',
        required: true,
        message: 'Name is required.',
        match: /^.{1,30}$/
      },
      description: {
        type: 'string',
        required: false,
        match: /^.{0,256}$/
      },
      status: {
        type: 'string',
        required: false,
        match: /^(done|inprogress|pending|cancel)$/,
      }
    });

module.exports = function(dba) {
    const obj = {
    tableName: 'tasks',
    validate(obj) {
      return validator.validate(obj);
    },
    isExist(taskId, callback) {
       dba.select('*')
         .from(this.tableName)
         .where({ id: taskId })
         .then(tasks => callback(null, tasks.length>0))
         .catch(err => callback(err));
    },
    list(callback) {
      dba.select('*')
        .from(this.tableName)
        .then(tasks => callback(null, tasks))
        .catch(err => callback(err));
    },
    get(taskId, callback) {
      dba.select('*')
        .from(this.tableName)
        .where({ id: taskId })
        .then(task => callback(null, task[0]))
        .catch(err => callback(err));
    },
    insert(task, callback) {     
        task.create_date = new Date().toISOString();
        task.update_date = new Date().toISOString();

        dba.insert(task)
          .into(this.tableName)
          .then(result => {
            dba.select('*')
              .from(this.tableName)
              .where({ id: result[0] })
              .then(newTask => callback(null, newTask[0]))
              .catch(err => callback(err));
          })
          .catch(err => callback(err));
    },
    update(taskId, task, callback) {
        task.update_date = new Date().toISOString();

        dba(this.tableName)
          .where('id', taskId)
          .update(task)
          .then(() => {
            dba.select('*')
              .from(this.tableName)
              .where({ id: taskId })
              .then(newTask => callback(null, newTask[0]))
              .catch(err => callback(err))
            ;
          })
          .catch(err => callback(err));
    },
    updatestatus(taskId, obj, callback) {

        dba(this.tableName)
          .where('id', taskId)
          .update({ status: obj.status, update_date: new Date().toISOString()})
          .then(() => {
            dba.select('*')
              .from(this.tableName)
              .where({ id: taskId })
              .then(newTask => callback(null, newTask[0]))
              .catch(err => callback(err))
            ;
          })
          .catch(err => callback(err));
    },
    delete(taskId, callback) {

      dba(this.tableName)
        .where('id', taskId)
        .delete()
        .then(() => callback(null))
        .catch(err => callback(err))
      ;
    }
  };
  return obj;
}
