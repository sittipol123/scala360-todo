'use strict';

const dba = require('../models/dba');
var tasks = require('../models/tasks')(dba);

exports.getAll = function(req, res) {

  tasks.list(function(err, tasks) {
      if (err) {
         res.status(500).json(err);
         return;
      }

      return res.json(tasks);
  });
};

exports.create = function(req, res) {
  var result = tasks.validate(req.body);
  if(result.length>0)
  {
    res.status(400).json({error: true, message: 'Field invalid.' ,result: result});
    return;
  }
   
  tasks.insert(req.body,function(err, task) {
    if (err) {
      res.status(500).send(err);
      return;
    }

    res.json(task);
  });
};

exports.get = function(req, res) {

  tasks.isExist(req.params.taskId, function(err, isExist) {
  
    if (err) {
       res.status(500).send(err);
       return;
    }

    if(!isExist)
    {
      return res.status(204).json({message: 'Dosen\'t exist'});
    }
     
    tasks.get(req.params.taskId, function(err, task) {

      if (err) {
        res.status(500).send(err);
        return;
      }

      res.json(task);
    });
  });
 
};

exports.update = function(req, res) {

  var result = tasks.validate(req.body);
  if(result.length>0)
  {
    res.status(400).json({error: true , message: 'Field invalid.',result: result});
    return;
  }

  tasks.isExist(req.params.taskId, function(err, isExist) {
  
      if (err) {
        res.status(500).send(err);
      }
      if(!isExist)
      {
        return res.status(204).json({message: 'Dosen\'t exist'});
      }
      tasks.update(req.params.taskId, req.body, function(err, task) {
        if (err) {
          res.status(500).send(err);
          return;
        }

        res.json(task);
      });
  });
};

exports.updateStatus = function(req, res) {
  if(req.body.status==null || !(req.body.status == "done" || req.body.status == "inprogress" || req.body.status =="pending" || req.body.status == "cancel") )
  {
     res.status(400).json({error: true, message: 'Status dosen\'t support.'});
     return;
  }

  tasks.isExist(req.params.taskId, function(err, isExist) {
  
     if (err) {
          res.status(500).send(err);
          return;
     }

     if(!isExist) {
        res.status(204).json({error: true,message: 'Dosen\'t exist'});
        return;
     }

     tasks.updatestatus(req.params.taskId, req.body.status, function(err, task) {
      if (err) {
        res.status(500).send(err);
        return;
      }

      res.json(task);
    });
  });
}

exports.delete = function(req, res) {
 tasks.isExist(req.params.taskId, function(err, isExist) {
  
    if (err) {
     res.status(500).send(err);
    }

    if(!isExist) {
       res.status(404).json({message: 'Dosen\'t exist'});
       return;
    }

    tasks.delete(req.params.taskId, function(err, task) {
      if (err) {
        res.status(500).send(err);
      }

      res.status(204).json(task);
    });
 });
};