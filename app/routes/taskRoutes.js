'use strict';

module.exports = function(app, express) {
	var tasks = require('../controllers/tasksController');
	app.use(express.static('public'));
	app.route('/tasks')
	     /**
		 * @api {get} /tasks Get all task.
		 * @apiGroup tasks
		 * @apiSuccess {Object[]} tasks Task's list
		 * @apiSuccess {Number} tasks.id Task id
		 * @apiSuccess {String} tasks.name Task name 
		 * @apiSuccess {String} tasks.description Task description
		 * @apiSuccess {String} tasks.status [done,inprogress,pending,cancel]
		 * 
		 * @apiSuccessExample {json} Success
		 *    HTTP/1.1 200 OK
		 *    [{
		 *      "id": 1,
		 *      "name": "Coding",
		 *      "status": "done"
		 *      "updated_date": "2017-05-10T18:38:01.000Z",
		 *      "created_date": "2017-05-10T18:38:01.000Z"
		 *    }]
		 * @apiErrorExample {json} Get all task error
		 *    HTTP/1.1 500 Internal Server Error
		 */
		.get(tasks.getAll)

		 /**
		 * @api {post} /tasks Create a task
		 * @apiGroup Tasks
		 * @apiParam {String} name, Task name
		 * @apiParam {String} description, Task description
		 * @apiParam {String} status, [done,inprogress,cancel]
		 * @apiParamExample {json} Input
		 *    {
    	 *		"name": "Coding",
    	 *		"description": "Coding description",
   		 *		"status": "inprogress"
		 *    }
		 * 
		 * @apiSuccess {Number} Task.id Task id
		 * @apiSuccess {String} Task.name Task name
		 * @apiSuccess {String} tasks.status [done,inprogress,pending,cancel]
		 * @apiSuccess {Date} created_date Register date
		 * @apiSuccess {Date} updated_date Update date
		 * 
		 * @apiSuccessExample {json} Success
		 *    HTTP/1.1 200 OK
		 *    {
		 *      "id": 1,
		 *      "name": "Coding",
		 *		"description": "Coding description",
		 *      "status": "inprogress"
		 * 	    "created_date": "2017-05-10T18:38:01.000Z"
		 *      "updated_date": "2017-05-10T18:38:01.000Z",
		 *    }
		 * @apiErrorExample {json} Request is invalid.  
		 *    HTTP/1.1 400 Bad request
		 * @apiErrorExample {json} Task dosen't exist
		 *    HTTP/1.1 204 No Content 
		 * @apiErrorExample {json} Get task error.
		 *    HTTP/1.1 500 Internal Server Error
		 */
		.post(tasks.create);

	app.route('/tasks/:taskId')

		/**
		 * @api {get} /tasks/:id Get a task by Id
		 * @apiGroup Tasks
		 * @apiSuccess {Number} Task.id Task id
		 * @apiSuccess {String} Task.name Task name
		 * @apiSuccess {String} tasks.status [done,inprogress,pending,cancel]
		 * @apiSuccess {Date} created_date Register date
		 * @apiSuccess {Date} updated_date Update date
		 * 
		 * @apiSuccessExample {json} Success
		 *    HTTP/1.1 200 OK
		 *    {
		 *      "id": 1,
		 *      "name": "Coding",
		 *		"description": "Coding description",
		 *      "status": "inprogress"
		 * 	    "created_date": "2017-05-10T18:38:01.000Z"
		 *      "updated_date": "2017-05-10T18:38:01.000Z",
		 *    }
		 * @apiErrorExample {json} Task dosen't exist
		 *    HTTP/1.1 204 No Content 
		 * @apiErrorExample {json} Get task error.
		 *    HTTP/1.1 500 Internal Server Error
		 */
		.get(tasks.get)

		/**
		 * @api {put} /tasks/:id Update a task
		 * @apiGroup Tasks
		 * @apiParam {String} name, Task name
		 * @apiParam {String} description, Task description
		 * @apiParam {String} status, [done,inprogress,pending,cancel]
		 * @apiParamExample {json} Input
		 *    {
		 *      "id": 1,
	     *		"name": "Coding,
		 *		"description": Coding description update",
		 *		"status": "cancel"
		 *    }
		 *
		 * @apiSuccessExample {json} Success
		 *    HTTP/1.1 200 OK
		 *    {
		 *      "id": 1,
		 *      "name": "Coding",
		 *		"description": "Coding description update",
		 *      "status": "cancel"
		 * 	    "created_date": "2017-05-10T18:38:01.000Z"
		 *      "updated_date": "2017-05-11T18:38:01.000Z",
		 *    }
		 * @apiErrorExample {json} Request is invalid.  
		 *    HTTP/1.1 400 Bad request
		 * @apiErrorExample {json} Task dosen't exist
		 *    HTTP/1.1 204 No Content 
		 * @apiErrorExample {json} Update error
		 *    HTTP/1.1 500 Internal Server Error
		 */
		.put(tasks.update)

		 /**
		 * @api {patch} /tasks Update a task status.
		 * @apiGroup Tasks
		 * @apiParam {String} status, [done,inprogress,pending,cancel]
		 * @apiParamExample {json} Input
		 *    {
   		 *		"status": "inprogress"
		 *    }
		 * 
		 * @apiSuccess {Number} Task.id Task id
		 * @apiSuccess {String} Task.name Task name
		 * @apiSuccess {String} tasks.status
		 * @apiSuccess {Date} created_date Register date
		 * @apiSuccess {Date} updated_date Update date
		 * 
		 * @apiSuccessExample {json} Success
		 *    HTTP/1.1 200 OK
		 *    {
		 *      "id": 1,
		 *      "name": "Coding",
		 *       "status": "inprogress"
		 * 	    "created_date": "2017-05-10T18:38:01.000Z"
		 *      "updated_date": "2017-05-10T18:38:01.000Z",
		 *    }
		 * @apiErrorExample {json} Request is invalid.  
		 *    HTTP/1.1 400 Bad request
		 * @apiErrorExample {json} Task dosen't exist
		 *    HTTP/1.1 204 No Content 
		 * @apiErrorExample {json} Get task error.
		 *    HTTP/1.1 500 Internal Server Error
		 */
		.patch(tasks.updateStatus)
		/**
		 * @api {delete} /tasks/:id Remove a task
		 * @apiGroup Tasks
		 * @apiParam {id} id Task id
		 * @apiSuccessExample {json} Success
		 *    HTTP/1.1 204 No Content
		 * @apiErrorExample {json} Task dosen't exist
		 *    HTTP/1.1 404 Not found
		 * @apiErrorExample {json} Delete error
		 *    HTTP/1.1 500 Internal Server Error
		 */
		.delete(tasks.delete);
};