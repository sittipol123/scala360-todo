# README #

Todo API on Nodejs express.

###Prerequisities###

| Prerequisite    | How to check | How to install
| --------------- | ------------ | ------------- |
| Node.js >= 4.5  | `node -v`    | [n - Node version manager](https://github.com/tj/n) |
| gulp >= 3.8.10  | `gulp -v`    | `npm install -g gulp` |
| MySql >= 5.6 | `mysql -V`   |  |



## How do I get set up? ##


###### Install packages

    npm install

###  Setup Database ###

Create database in Mysql and configuration a `.env` file on root.

```
#!javascript
ex
DEV_DB_CLIENT=mysql
DEV_DB_HOST=127.0.0.1
DEV_DB_USER=root
DEV_DB_PASS=xxxxx
DEV_DB_NAME=scalatodo

```
###### Migrate database

    knex migrate:latest

###  Start developing ###

###### Run server

    gulp dev

### API Doc ###

###### Build API Doc

    gulp apidoc

Open http://localhost:3000/apidoc/

###### Use the postman.

   Import a postman file from `./postman/`

   Set environment `url=http://localhost:3000`