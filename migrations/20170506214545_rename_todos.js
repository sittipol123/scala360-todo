
exports.up = function(knex, Promise) {
  return knex.schema.renameTable('todos', 'tasks')
};

exports.down = function(knex, Promise) {
  return knex.schema.renameTable('tasks', 'todos')
};
