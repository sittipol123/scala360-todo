require('dotenv').config();

exports.up = function(knex, Promise) {

  return knex.schema.createTable('todos', table => {
    table.increments('id').primary()
    table.string('name').notNullable()
    table.string('description')
    table.string('status')
    table.dateTime('create_date')
    table.dateTime('update_date')
  })
};

exports.down = function(knex, Promise) {
  return knex.schema.dropTableIfExists('todos')
};
