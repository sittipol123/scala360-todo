require('dotenv').config();
var express = require('express');
var app = express();
var fs = require("fs");
var port = process.env.API_PORT || 3000;
var bodyParser = require('body-parser');
var taskRoutes = require('./app/routes/taskRoutes');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

taskRoutes(app,express);

app.use(function(req, res) {
  res.status(404).send({url: req.originalUrl + ' not found'})
});

app.listen(port);

console.log('Server todpd API running on port'+port);