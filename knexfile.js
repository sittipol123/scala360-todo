'use strict';

// Update with your config settings.
require('dotenv').config();

module.exports = {
    client: process.env.DEV_DB_CLIENT,
    connection: {
      host : process.env.DEV_DB_HOST,
      user : process.env.DEV_DB_USER,
      password : process.env.DEV_DB_PASS,
      database : process.env.DEV_DB_NAME
    }
};
