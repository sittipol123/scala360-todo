require('dotenv').config();
var gulp = require('gulp');
var nodemon = require('gulp-nodemon');
var apidoc = require('gulp-api-doc');

gulp.task('dev', function (cb) {
  var stream = nodemon({ 
                         script: 'server.js',
                         ext: 'js',
                         ignore: ['node_modules/**/*.js'],
                       });

  stream.on('restart', function () {
            console.log('restarted!');
        })
        .on('crash', function() {
            console.error('Application has crashed!\n');
            stream.emit('restart', 10);  // restart the server in 10 seconds 
        });
});

gulp.task('apidoc', function (cb) {
    return gulp.src('app')
        .pipe(apidoc())
        .pipe(gulp.dest('public/apidoc'));
});